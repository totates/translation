import logging from "external:@totates/logging/v1";
import { negotiateLanguages } from "@fluent/langneg";
import { FluentBundle, FluentResource } from "@fluent/bundle";

export { negotiateLanguages, FluentBundle, FluentResource };

const logger = logging.getLogger("@totates/translation/v1");
// logger.setLevel("DEBUG");

logger.debug(`navigator languages: ${navigator.languages}`);

class Resolver {
    constructor(resourceId, availableLocales, resolve) {
        this.resourceId = resourceId;
        this.resolve = resolve;
        this.availableLocales = availableLocales;
        this.bundles = {};
        /*
        this.supportedLocales = negotiateLanguages(
            navigator.languages, availableLocales, { defaultLocale: "en-US" }
        );
        logger.debug(`${resourceId}: available ${availableLocales}; negotiated: ${this.supportedLocales}`);
        */
    }

    async getBundle(language) {
        let bundle = this.bundles[language];
        if (!bundle) {
            const negotiatedLanguages = negotiateLanguages(
                [language], this.availableLocales, { defaultLocale: "en-US" }
            );
            const negotiated = negotiatedLanguages[0];
            logger.debug(`${this.resourceId}: negotiated language for ${language} is ${negotiated}`);

            const url = this.resolve(negotiated);
            logger.debug(`${this.resourceId} bundle for ${negotiated} at ${url}`);
            const response = await fetch(url);
            if (!response.ok)
                throw new Error(`could not fetch bundle at ${url}`);
            const ftl = await response.text();
            const resource = new FluentResource(ftl);
            bundle = new FluentBundle(negotiated);
            const errors = bundle.addResource(resource);
            if (errors.length)
                logger.warn(`errors were found in ${this.resourceId} bundle for ${negotiated}: ${errors}`);
            this.bundles[negotiated] = bundle;
        }
        return bundle;
    }
}

export function getLanguage() {
    return navigator.language;
}

const setDocumentLang = () => document.documentElement.lang = getLanguage();
window.addEventListener("languagechange", setDocumentLang);
setDocumentLang();

const resolvers = {};

export function registerBundleResolver(resourceId, availableLocales, resolve) {
    resolvers[resourceId] = new Resolver(resourceId, availableLocales, resolve);
    logger.debug(`registered bundle resolver for ${resourceId}`);
}

export function registerElementBundleResolver(index, url) {
    const baseUrl = url.slice(0, url.lastIndexOf("/"));
    const resolve = language => `${baseUrl}/locales/${language}.ftl`;
    registerBundleResolver(index.tagName, index.languages, resolve);
}

export function getBundleResolver(resourceId) {
    return resolvers[resourceId];
}

export default { getBundleResolver, getLanguage, registerBundleResolver };
